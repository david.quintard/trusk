## Description

Technical test for Trusk.

## Installation

```bash
$ npm install
```

## Running the app

```bash
$ node inquirer.js
$ node redis.js
```

## Stay in touch

- Author - [David Quintard](david.quintard@gmail.com)