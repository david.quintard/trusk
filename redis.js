const inquirer = require('inquirer');
const redis = require('redis');
const Promise = require("bluebird");

const run = async () => {
    const client = redis.createClient();
    await client.connect();

    client.on('error', (err) => console.log('Redis Client Error', err));

    const promptTrusker = async () => {
        const answer = await inquirer.prompt([
            {
                type: "input",
                name: "trusker",
                message: "Quel est le nom du trusker ?",
                default: await client.get('trusker') ?? ''
            }
        ]);

        if(answer.trusker === '') {
            return await promptTrusker();
        } else {
            await client.set('trusker', answer.trusker);
            return answer;
        }
    }
    const promptSociete = async () => {
        const answer = await inquirer.prompt([
            {
                type: "input",
                name: "societe",
                message: "Quel est le nom de la société ?",
                default: await client.get('societe') ?? ''
            }
        ]);

        if(answer.societe === '') {
            return await promptSociete();
        } else {
            await client.set('societe', answer.societe);
            return answer;
        }
    }
    const promptNombreEmployes = async () => {
        const answer = await inquirer.prompt([
            {
                type: "number",
                name: "nombre_employes",
                message: "Quel est le nombre d'employés ?",
                default: await client.get('nombre_employes') ?? ''
            }
        ]);

        if(isNaN(answer.nombre_employes)) {
            return await promptNombreEmployes();
        } else {
            await client.set('nombre_employes', answer.nombre_employes.toString());
            return answer;
        }
    }
    const promptNomEmploye = async (i) => {
        let numero = i + 1;
        const answer = await inquirer.prompt([
            {
                type: "input",
                name: "employe_nom_" + numero,
                message: "Quel est le nom de l'employé " + numero,
                default: await client.get('employe_nom_' + numero) ?? ''
            }
        ]);

        if(answer["employe_nom_" + numero] === '') {
            return await promptNomEmploye(i);
        } else {
            await client.set('employe_nom_' + numero, answer["employe_nom_" + numero]);
            return answer;
        }
    }
    const promptNombreCamions = async () => {
        const answer = await inquirer.prompt([
            {
                type: "number",
                name: "nombre_camions",
                message: "Quel est le nombre de camions ?",
                default: await client.get('nombre_camions') ?? ''
            }
        ]);

        if(isNaN(answer.nombre_camions)) {
            return await promptNombreCamions();
        } else {
            await client.set('nombre_camions', answer.nombre_camions.toString());
            return answer;
        }
    }
    const promptVolumeCamion = async (i) => {
        let numero = i + 1;
        const answer = await inquirer.prompt([
            {
                type: "number",
                name: "camion_vol_" + numero,
                message: "Quel est le volume (m3) du camion " + numero,
                default: await client.get('camion_vol_' + numero) ?? ''
            }
        ]);

        if(isNaN(answer["camion_vol_" + numero])) {
            return await promptVolumeCamion(i);
        } else {
            await client.set('camion_vol_' + numero, answer["camion_vol_" + numero].toString());
            return answer;
        }
    }
    const promptTypeCamion = async (i) => {
        let numero = i + 1;
        const answer = await inquirer.prompt([
            {
                type: "input",
                name: "camion_type_" + numero,
                message: "Quel est le type du camion " + numero,
                default: await client.get('camion_type_' + numero) ?? ''
            }
        ]);

        if(answer["camion_type_" + numero] === '') {
            return await promptTypeCamion(i);
        } else {
            await client.set('camion_type_' + numero, answer["camion_type_" + numero]);
            return answer;
        }
    }

    await promptTrusker();
    await promptSociete();
    const answerEmployes = await promptNombreEmployes();

    const employes = Array.from(Array(answerEmployes.nombre_employes).keys());
    await Promise.each(employes, async function(employe, i, arrayLength) {
        // The iteration will be performed sequentially, awaiting for any
        // promises in the process.
        await promptNomEmploye(i);
    });

    const answerCamions = await promptNombreCamions();

    const camions = Array.from(Array(answerCamions.nombre_camions).keys());
    await Promise.each(camions, async function(camion, i, arrayLength) {
        // The iteration will be performed sequentially, awaiting for any
        // promises in the process.
        await promptVolumeCamion(i);
        await promptTypeCamion(i);
    });

    let isValid = (
        await inquirer.prompt([
            {
                type: "confirm",
                name: "repeat",
                message: "Les informations sont elles valides?",
            },
        ])
    ).repeat;

    if(!isValid) {
        await run();
    }
    await client.quit();
}

run().then(r => {
    console.log("fini");
}).catch(error => {
    console.log(error);
});